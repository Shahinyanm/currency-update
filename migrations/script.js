import { default as pg } from 'pg'
import { default as Postgrator } from 'postgrator'
import { dirname } from "path";
import { fileURLToPath  } from 'url'

const __dirname = dirname(fileURLToPath(import.meta.url));

async function main() {
    const client = new pg.Client({
        host: "localhost",
        port: 5432,
        database: "test_task",
        user: "postgres",
        password: "postgres",
    });

    try {
        // Establish a database connection
        await client.connect();

        // Create postgrator instance
        const postgrator = new Postgrator({
            migrationPattern: __dirname + '/sql/*',
            driver: "pg",
            database: "test_task",
            schemaTable: "schemaversion",
            execQuery: (query) => client.query(query),
        });

        // Migrate to specific version
        const appliedMigrations = await postgrator.migrate();
        console.log( appliedMigrations);

        // Or migrate to max version (optionally can provide 'max')
        await postgrator.migrate();
    } catch (error) {
        // If error happened partially through migrations,
        // error object is decorated with appliedMigrations
        console.error(error.appliedMigrations); // array of migration objects
    }

    // Once done migrating, close your connection.
    await client.end();
}
main();

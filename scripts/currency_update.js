import fastify from 'fastify';
import 'dotenv/config'
import fastifyPostgres from '@fastify/postgres';
import {xml2json} from 'xml-js'

const app = fastify();

await app.register(fastifyPostgres, {
    connectionString: `postgresql://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}/${process.env.DB_NAME}`,
    max: 10,
    idleTimeoutMillis: 30000
});

const TARGET_URL = process.env.CURRENCY_LIST_URL;
async function main() {
    const data = await getNewCurrencies();
    const normalizedData = normalizeCurrencyDataForDBOperations(data);

    const res = ((await app.pg.query('SELECT COUNT(*) FROM currency')).rows[0]);

    if (parseInt(res.count, 10) === 0) {
        await insertCurrencies(normalizedData);
        process.exit(0);
    }

    await updateCurrencies(normalizedData);

    process.exit(0);
}

async function insertCurrencies(insertData) {
    await app.pg.query(`
        INSERT INTO currency(name, rate) VALUES ${insertData}
    `)
}

async function updateCurrencies(updateData) {
    const rawQuery = `
        UPDATE currency SET rate = new_date.rate
        FROM (VALUES ${updateData} ) AS new_date(name, rate)
        WHERE new_date.name = currency.name;
    `
    await app.pg.query(rawQuery);
}


function normalizeCurrencyDataForDBOperations(currenciesData) {
    return currenciesData.map(entity => {
        const currencyCode = (entity.elements.find(item => item.name === 'title')).elements[0].text;

        const quantity = (entity.elements.find(item => item.name === 'quant')).elements[0].text;
        const value = (entity.elements.find(item => item.name === 'description')).elements[0].text;
        const currencyRate = (parseFloat(value) / parseInt(quantity, 10)).toFixed(3);

        return `('${currencyCode}', ${currencyRate})`
    });
}

function getNewCurrencies() {
    return fetch(TARGET_URL)
        .then(res => res.text())
        .then(res => {
            const jsonData = JSON.parse(xml2json(res));
            return jsonData.elements[0].elements[0].elements.filter(element => element.name === 'item')
        })
        .catch(err => {
            console.error('ERR: ', err.message);
            process.exit(1);
        })
}

main();

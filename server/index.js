import fastify from 'fastify';
import fastifyPostgres from '@fastify/postgres';
import * as fastifyJwt from '@fastify/jwt'
import 'dotenv/config'


import authMiddleware from './middlewares/jwt-auth.js';

const app = fastify();
await app.register(fastifyPostgres, {
    connectionString: 'postgresql://postgres:postgres@localhost:5432/test_task',
    max: 10,
    idleTimeoutMillis: 30000
})
await app.register(fastifyJwt, {
    secret: 'jwtSecretKey123456',
});


app.get('/currencies', {
    onRequest: authMiddleware
}, async function (request, reply) {
    const { page, size } = request.query

    const parsedPage = parseInt(page, 10);
    const parsedSize = parseInt(size, 10);

    const offsetPage = parsedPage ? (parsedSize < 1 ? 1 : parsedPage) : 1;
    const pageSize = parsedSize ? (parsedSize < 5 ? 5 : parsedSize) : 5;

    const data = await app.pg.query(`SELECT name, rate FROM currency ORDER BY name OFFSET $1 LIMIT $2`, [(offsetPage-1) * pageSize, pageSize]);
    return data.rows;
})

app.get('/currency/:currencyId', {
    onRequest: authMiddleware
}, async function (request, reply) {
    const { currencyId } = request.params;
    const data = await app.pg.query(`SELECT name, rate FROM currency WHERE id=$1`, [parseInt(currencyId, 10)]);
    return data.rows[0];
})

app.listen({port: process.env.APP_PORT}, function (err, address) {
    if (err) {
        app.log.error(err)
        process.exit(1)
    }
    console.log(`Listening ${process.env.APP_PORT}`)
    // Server is now listening on ${address}
})